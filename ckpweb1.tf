provider "aws" {
 access_key = "${var.aws_access_key}"
 secret_key = "${var.aws_secret_key}"
 ssh_key_name = "${var.ssh_key_name}"
 ssh_key_path = "${var.ssh_key_path}"
# profile = admin1
 region     = "ap-south-1"

}




#*******TEST*********
#data "terraform_remote_state" "shared-state"{
#    provider = "aws.admin"
#    backend = "s3"
#    config {
#        bucket = "${var.state-bucket}"
#        key = "${var.group}/${var.project}/${var.environment}.tfstate"
#        region = "${var.state-region}"
#        profile = "admin"
#    }
#}


#resource "aws_s3_bucket" "ckpweb1" {
  # ...
#}

#resource "aws_s3_bucket" "b" {
#    bucket = "my_tf_test_bucket"
#    acl = "private"
#    versioning {
#        enabled = true




#resource "aws_iam_user" "admin1" {
#  name = "admin1"
#  path = "/home/"
#}


resource "aws_instance" "ckpweb1" {
  ami   = "ami-cdbdd7a2"
  count = 1
  instance_type = "t2.micro"
  security_groups = ["sg-c963f8a0"]
  subnet_id = "subnet-46d5642f"
  source_dest_check = true
  key_name = "ckprhel1"
  root_block_device {
    volume_size =  30
    volume_type =  "standard"
    delete_on_termination = "false"
  }
  tags {
       Name = "RHEL-CKPWEB1"
 }  
 
 provisioner "remote-exec" {
   connection {
            type = "ssh"
            user = "ec2-user"
            key_file = "${var.ssh_key_name}"
			
     inline = [
    "sudo yum -y install java-1.8.0-openjdk",
	"sudo yum -y install git",
	"sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo",
	"sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key",
	"sudo yum -y install jenkins",
	"sudo service jenkins start/stop/restart",
	"sudo chkconfig jenkins on"  
      ]
   }
 
}

#data "terraform_remote_state" "ckpweb1" {
#    provider = "aws.admin1"
#    backend = "s3"
#    profile = "admin1"
#    config {
#        bucket = "ckp1"
#        key = "tfstate/terraform.tfstate"
#        region = "us-east-1"
 #   }
#}
}